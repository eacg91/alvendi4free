import java.util.ArrayList;
import java.sql.*;

public class funcionesMySQL {
    /*
    String userMySQL="root";
    String passMySQL="**********";
    String hostMySQL="localhost";
    String BaseDatos="computaccion";
    */
    int puertoMySQL = 3306;
    protected Connection conn = null;
    protected Statement stmt = null;
    int numberOfColumns;
	
    public void DROP(String que){
		try{
			stmt.executeUpdate("DROP "+que+";");
		}  catch (Exception f) {
			f.printStackTrace();
		}
	}
    public void CREATE(String que) throws SQLException {
		try{
			stmt.executeUpdate("CREATE "+que+";");
		}  catch (Exception f) {
			f.printStackTrace();
		}
	}
    public void DELETE(String table,String donde){
		try{
			stmt.executeUpdate("DELETE FROM "+table+" WHERE "+donde+";");
		}  catch (Exception f) {
			f.printStackTrace();
		}
	}
    public void UPDATE(String table,String nuevo,String donde){
		try{
			stmt.executeUpdate("UPDATE "+table+" SET "+nuevo+" WHERE "+donde+";");
		}  catch (Exception f) {
			f.printStackTrace();
		}
	}
    public void INSERT(String table,String values){
		try{
			stmt.execute("INSERT IGNORE INTO "+table+" VALUES "+values+";");
		}  catch (Exception f) {
			f.printStackTrace();
		}
	}
    public String SELECT(String campos,String tablasYfiltros){
		String cadena="";
		try{
			ResultSet rs = stmt.executeQuery ("SELECT "+campos+" FROM "+tablasYfiltros+";");
			ResultSetMetaData rsmd = rs.getMetaData();
			int numberOfColumns = rsmd.getColumnCount();
			while (rs.next()) {
				if(!cadena.equals(""))	cadena+="\n";
				for(int i=1;i<=numberOfColumns;i++){
					if(i>1)
						cadena+=" ";
					/*if( i==1 && (rs.getString(i).equals("null")|rs.getString(i).equals("NULL")) )
						cadena+=" ";
					else*/
						cadena+=rs.getString(i);
				}
			}
		} catch (SQLException SQLE) {
			;
		}
		return cadena;
    }
	
	public void conectar(){
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			this.conn = DriverManager.getConnection("jdbc:mysql://"+hostMySQL+":"+puertoMySQL+"/"+BaseDatos,userMySQL,passMySQL);
			this.stmt = conn.createStatement();
		}  catch (Exception f) {
			f.printStackTrace();
		}
	}
	public boolean conectar(String userMySQL,String passMySQL,String hostMySQL,String BaseDatos) {
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			this.conn = DriverManager.getConnection("jdbc:mysql://"+hostMySQL+":"+puertoMySQL+"/"+BaseDatos,userMySQL,passMySQL);
			this.stmt = conn.createStatement();
			return true;
		}  catch (Exception f) {
			f.printStackTrace();
		}
		return false;
	}
	public void desconectar(){
		try{
			if (this.stmt != null) {
				try {
					this.stmt.close();
				} catch (SQLException SQLE) {
					;
				}
			}
			if (this.conn != null) {
				try {
					this.conn.close();
				} catch (SQLException SQLE) {
					;
				}
			}
		}  catch (Exception f) {
			f.printStackTrace();
		}
	}
    /**************************************************************************/
    public int numeroColumnas(){
        return numberOfColumns;
    }
    
    public ArrayList<String> SELECTlist(String campos,String tablasYfiltros){
		ArrayList<String> lista = new ArrayList<String>();
		try{
			ResultSet rs = stmt.executeQuery ("SELECT "+campos+" FROM "+tablasYfiltros+";");
			ResultSetMetaData rsmd = rs.getMetaData();
			numberOfColumns = rsmd.getColumnCount();
			while (rs.next()) {
				for(int i=1;i<=numberOfColumns;i++){
					/*if(rs.getString(i).equals("null")|rs.getString(i).equals("NULL"))
						lista.add(" ");
					else*/
						lista.add(rs.getString(i));
				}
			}
		} catch (SQLException SQLE) {
			;
		}
		return lista;
    }
	
    public int SELECT_ID(String id,String tablasYfiltros){
		try{
			ResultSet rs = stmt.executeQuery ("SELECT "+id+" FROM "+tablasYfiltros+";");
			while (rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException SQLE) {
			;
		}
		return -1;
    }
    
    public boolean SELECT_existe(String tablasYfiltros){
        int contar = Integer.parseInt(SELECT("count(*)",tablasYfiltros));
        if(contar>0)
            return true;
        return false;
    }
	
    public ArrayList<String> fechaYhora(){
		ArrayList<String> lista = new ArrayList<String>();
		try{
			ResultSet rs = stmt.executeQuery ("SELECT CURDATE();");
			ResultSetMetaData rsmd = rs.getMetaData();
			while (rs.next()) {
				lista.add(rs.getString(1));
			}
			rs = stmt.executeQuery ("SELECT CURTIME();");
			rsmd = rs.getMetaData();
			while (rs.next()) {
				lista.add(rs.getString(1));
			}
		} catch (SQLException SQLE) {
			;
		}
		return lista;
    }
}
