package Ejemplos.Imagen1;

import <any?>;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Principal extends JFrame {
	private static final long serialVersionUID = 1L;
	
	public Principal() {
		super("Muestra de Imagen en JAVA...");
		this.setSize(600,450);
		JLabel lblFoto = new JLabel();
		lblFoto.setBounds(this.getWidth()-300,this.getHeight()-270,300,135);
		
		//String url = "http://k31.kn3.net/taringa/1/1/7/6/4/5/44/diazespina/5B9.jpg";
		//Image img = getImage(new URL(url));
		
		ImageIcon imagenIcono = new ImageIcon();
		Icon icono = new ImageIcon(imagenIcono.getImage().getScaledInstance(lblFoto.getWidth(),lblFoto.getHeight(),Image.SCALE_DEFAULT));
		lblFoto.setIcon(icono);
		//lblFoto.setIcon(new ImageIcon(imagen));
		this.add(lblFoto);
		
		//JLabel etiqueta = new JLabel(imagen); //Sirve como fondo ?
		//etiqueta.setBounds(1,1,300,135);
		//this.add(etiqueta);
	}
	
	public static void main(String H[]) {
		Principal p = new Principal();
		p.setVisible(true);
 
		//COLOCAMOS EL CODIGO QUE PERMITE CERRAR LA VENTANA
		p.addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent evt)
			{
				System.exit(0);
			}
		});
	}
}
