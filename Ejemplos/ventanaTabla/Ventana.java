import java.awt.*;
import javax.swing.*;

public class Ventana extends JFrame{
	Object[][] data = {
                                {"Mary", "Campione", "Esquiar", new Integer(5), new Boolean(false)},
                                {"Lhucas", "Huml", "Patinar", new Integer(3), new Boolean(true)},
                                {"Kathya", "Walrath", "Escalar", new Integer(2), new Boolean(false)},
                                {"Marcus", "Andrews", "Correr", new Integer(7), new Boolean(true)},
                                {"Angela", "Lalth", "Nadar", new Integer(4), new Boolean(false)}
                       };
	String[] columnNames = {"Nombre", "Apellido", "Pasatiempo","Años de Practica", "Soltero(a)"};
	JTable table = new JTable(data,columnNames);
	
	public static void main(String []args) {
		new Ventana();
	}
	
	public Ventana() {
		setTitle("Tabla en una Ventana");
		getContentPane().setLayout(null);
		
		//table.setPreferredScrollableViewportSize(new Dimension(500, 70));
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(50,200,500,70);
		getContentPane().add(scrollPane);
		
		setSize(600,500);
		show();
	}
}
