
import javax.swing.*;
import java.awt.*;
import javax.swing.table.*;

import java.util.*;

import java.io.*;
public class jtablejlist //extends JFrame
{

public void init()
{
Vector data = new Vector();
Vector tmp = new Vector();

String[] sdata ={"test1","test2"};
tmp.addElement (sdata);
tmp.addElement ("This is test1");
data.addElement (tmp);

tmp = new Vector();
String[] sdata2 ={"stock1","stock2","stock3","stock4"};
tmp.addElement (sdata2);
tmp.addElement ("This is stocks");
data.addElement (tmp);


Vector cols = new Vector();
cols.addElement ("col1");
cols.addElement ("col2");

JTable table = new JTable(data,cols);
TableColumn column = table.getColumnModel().getColumn(0);
MyListEditor area=new MyListEditor();
// ROWHIGHT=table.getRowHeight() +table.getRowMargin()+1; 
column.setCellEditor(area); 
column.setCellRenderer(new MyCellRenderer ());

JFrame frame = new JFrame();
frame.getContentPane().add(new JScrollPane(table));
frame.pack();
frame.setVisible(true);
}

class MyListEditor extends AbstractCellEditor implements TableCellEditor
{ 
JList mlist;

public MyListEditor()
{
mlist = new JList();
}

public Object[] getCellEditorValue() 
{
return ((DefaultListModel)mlist.getModel()).toArray();
}

// This method is called when a cell value is edited by the user.
public Component getTableCellEditorComponent(JTable table, 
Object value, boolean isSelected, int row,int column) 
{ 
mlist.setListData((Object[])value);
return mlist;
}

}

class MyCellRenderer extends JList implements TableCellRenderer 
{
public MyCellRenderer(){
super();
}
public Component getTableCellRendererComponent(JTable mtable, Object value,
boolean isSelected, boolean hasFocus, int rowIndex, int vColIndex) {

if(isSelected)
setBackground(mtable.getSelectionBackground() );
else
setBackground(Color.white );
String stmp;
StringTokenizer st;
int rowHight=mtable.getRowHeight() +mtable.getRowMargin()+1;
int fold;

String[] mlist = (String[]) value;


fold = mlist.length ;

//adjust row height
if(mtable.getRowHeight(rowIndex)!=fold*rowHight) //otherwise, the save dialog couldn't show normally 
mtable.setRowHeight(rowIndex, fold*rowHight); 
setListData((String[])value);

return this;
} 
}
public static void main(String[] args)
{
jtablejlist jj =new jtablejlist();
jj.init();
}

}
