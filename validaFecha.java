/**
 * @author eacg91
 */
import java.util.Calendar;

public class validaFecha {
    Calendar cal = Calendar.getInstance();
    private int dia=1,mes=1,anio=1,MAXdia=28;
    public final int ENERO=1,FEBRERO=2,MARZO=3,ABRIL=4,MAYO=5,JUNIO=6,JULIO=7;
    private final int AGOSTO=8,SEPTIEMBRE=9,OCTUBRE=10,NOVIEMBRE=11,DICIEMBRE=12;
		
	private boolean verificaDia(int d) {
		if(d>0){
			switch(mes){
				case ENERO:
					MAXdia=31;
					break;
				case FEBRERO:
					if(verificaBisiesto(anio))
						MAXdia=29;
					else
						MAXdia=28;
					break;
				case MARZO:
					MAXdia=31;
					break;
				case ABRIL:
					MAXdia=30;
					break;
				case 5:
					MAXdia=31;
					break;
				case 6:
					MAXdia=30;
					break;
				case 7:
					MAXdia=31;
					break;
				case 8:
					MAXdia=31;
					break;
				case 9:
					MAXdia=30;
					break;
				case 10:
					MAXdia=31;
					break;
				case 11:
					MAXdia=30;
					break;
				case 12:
					MAXdia=31;
					break;
			};
                        if(d<=MAXdia){
				return true;
			}
		}
		return false;
	}
	public boolean verificaBisiesto(int a) {
		if((a%4)==0){
			if( (a%100)!=0 | (a%400)==0 ){
				return true;
			}
		}
		return false;
	}
		
	public int dameDia( ) {
		return dia;
	}
	public int dameMes( ) {
		return mes;
	}
	public int dameAnio( ) {
		return anio;
	}
	public int dameMaxDia( ) {
		return MAXdia;
	}
		
	public boolean fijaDia(int d) {
		if(verificaDia(d)){
			dia = d;
			return true;
		}
		return false;
	}
	public boolean fijaMes(int m) {
            int aux = mes;
            if( m>0 && m<=12 ){
                mes = m;
                if(verificaDia(dia))
                    return true;
            }
            mes = aux;
            return false;
	}
	public boolean fijaAnio(int a) {
		if( mes==2 && dia==29 && !verificaBisiesto(a) ) {
                    MAXdia=28;
			return false;
		}
		anio = a;
		return true;
	}
        public void fijarFechaActual() {
            fijaAnio(cal.get(Calendar.YEAR));
            fijaMes(cal.get(Calendar.MONTH)+1);
            fijaDia(cal.get(Calendar.DAY_OF_MONTH));
        }
}
