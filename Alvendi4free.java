/*		Edwin Alberto Castañeda Garcia		*/
//package Alvendi4free;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
//

class elementosGraficos extends JFrame {
	private static final long serialVersionUID = 1L;
	public JLabel lblImagen(JLabel lbl,String rutaImagen,int x,int y,int ancho,int altura){
		lbl.setBounds(x,y,ancho,altura);
		ImageIcon imagenIcono = new ImageIcon(rutaImagen);
		Icon icono = new ImageIcon(imagenIcono.getImage().getScaledInstance(lbl.getWidth(),lbl.getHeight(),Image.SCALE_DEFAULT));
		lbl.setIcon(icono);
		return lbl;
	}
	public JLabel lblImagen(String rutaImagen,int x,int y,int ancho,int altura){
		JLabel lbl = new JLabel();
		return lblImagen(lbl,rutaImagen,x,y,ancho,altura);
	}
	public void descargaImagen(String URLimagen,String destino) {	// destino = /dir/imagen.jpg
		//	http://chuwiki.chuidiang.org/index.php?title=Descargar_ficheros_web_con_Java
        try {
            java.net.URL url = new java.net.URL(URLimagen);
            java.net.URLConnection urlCon = url.openConnection();
            System.out.println(urlCon.getContentType());	// Sacamos por pantalla el tipo de fichero
            java.io.FileOutputStream fos;	
            try (java.io.InputStream is = urlCon.getInputStream() // Se obtiene el inputStream de la foto web y se abre el fichero local.
            ) {
                fos = new java.io.FileOutputStream(destino); // Lectura de la foto de la web y escritura en fichero local
                byte[] array = new byte[1000];		// buffer temporal de lectura.
                int leido = is.read(array);
                while (leido > 0) {
                    fos.write(array, 0, leido);
                    leido = is.read(array);
                }
            } // Lectura de la foto de la web y escritura en fichero local
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //	http://baro3495.blogspot.mx/2013/04/como-mostrar-un-dialogo-de-mensaje-en.html
    public void WARNING(String txt){
		JOptionPane.showMessageDialog(null,txt,"WARNING !",JOptionPane.WARNING_MESSAGE);
	}
    public void ERROR(String txt){
		JOptionPane.showMessageDialog(null,txt,"ERROR !",JOptionPane.ERROR_MESSAGE);
	}
    public void INFO(String txt){
		JOptionPane.showMessageDialog(null,txt,"INFO",JOptionPane.INFORMATION_MESSAGE);
	}
	//Se pretende incluir otras funciones para reducir lineas de codigo repetitivo en aplicaciones graficas
}

class Alvendi4free_gui extends JFrame implements ActionListener/*, ChangeListener */{
	private static final long serialVersionUID = 1L;
	validaFecha fecha=new validaFecha();
	miFecha miFecha=new miFecha();
	funcionesMySQL db = new funcionesMySQL();
	elementosGraficos elemGraf=new elementosGraficos();
	String DIR_IMG = "/home/eacg91/Documentos/lighttpd/computaccion/";	// Debe cambiar '/' por '\' en Microsoft Windows
	int alto,ancho,imgAlto,imgAncho;
	/*		Componentes de Menú 		*/
	JMenuBar barraMenu;
	JMenu menuAdmin,menuInventario,menuVentas,menuEnvios,menuIdentidades,menuReportes;
	JMenuItem item_Salir;
	JMenuItem item_listaInventario,item_ConsultaIDproducto,item_BuscarProducto,item_Capturar;
	JMenuItem item_Carrito,item_ConsultaOrden,item_OrdenesCliente,item_OrdenesEmpleado;
	JMenuItem item_envioDeProveedor,item_envioAlCliente,item_AgregarRastreo;/*				*/
	JMenuItem item_login,item_registraEmpleado,item_registraCliente,item_registraEmpresaCliente,item_registraProveedor;
	JMenuItem item_modificaEmpleado,item_modificaCliente,item_modificaEmpresaCliente,item_modificaProveedor;
	JTextField cajaID;
	JLabel lblID,lblNombre,lblActivo,lblImagen;
	JLabel lbl1,lbl2,lbl3,lbl4,lbl5,lbl6,lbl7,lbl8,lbl9,lbl10,lbl11;
	JComboBox comboTipos,comboVentas_medioEntrega,comboVentas_medioPago,comboEmpleadosVentas,comboClientesVentas;
	JButton btnBuscar,btnDetalle,btnMasDetalle,btnCarrito,btnCarritoExt,btnModificar,btnRegistrar;
	JButton btnRegistrarOrden,btnConsultarOrden;
	
	private void barraMenus(){
		this.setJMenuBar( barraMenu = new JMenuBar() );
			//
			barraMenu.add( menuAdmin = new JMenu("Programa") );
				menuAdmin.add( item_login = new JMenuItem("Log In") ).addActionListener(this);
				menuAdmin.add(/*item_Salir =*/new JMenuItem("Preferencias") ).addActionListener(this);
				menuAdmin.addSeparator();
				menuAdmin.add( item_Salir = new JMenuItem("Salir") ).addActionListener(this);
			//
			barraMenu.add( menuInventario = new JMenu("Productos") );
				menuInventario.add( item_listaInventario = new JMenuItem("Inventario") ).addActionListener(this);
				menuInventario.add( item_ConsultaIDproducto = new JMenuItem("Consultar ID") ).addActionListener(this);
				menuInventario.add( item_BuscarProducto = new JMenuItem("Buscar") ).addActionListener(this);
				menuInventario.add( item_Capturar = new JMenuItem("Capturar Nuevo") ).addActionListener(this);
			//
			barraMenu.add( menuVentas = new JMenu("Ventas") );
				menuVentas.add( item_Carrito = new JMenuItem("Carrito de Venta") ).addActionListener(this);
				menuVentas.add( item_ConsultaOrden = new JMenuItem("Consultar Folio de una Orden") ).addActionListener(this);
				menuVentas.add( item_OrdenesCliente = new JMenuItem("Ordenes de un Cliente") ).addActionListener(this);
				menuVentas.add( item_OrdenesEmpleado = new JMenuItem("Ordenes atendidas por un Empleado") ).addActionListener(this);
			//
			barraMenu.add( menuEnvios = new JMenu("Envios") );
				menuEnvios.add( item_envioDeProveedor = new JMenuItem("Rastrea Envios de Proveedor") ).addActionListener(this);
				menuEnvios.add( item_envioAlCliente = new JMenuItem("Rastrea Envios al Cliente") ).addActionListener(this);
				menuEnvios.add( item_AgregarRastreo = new JMenuItem("Agregar Rastreo") ).addActionListener(this);
			//
			barraMenu.add( menuIdentidades = new JMenu("Identidades") );
				menuIdentidades.add( item_login = new JMenuItem("Log In") ).addActionListener(this);
				menuIdentidades.add( item_registraCliente = new JMenuItem("Registrar Cliente") ).addActionListener(this);
				menuIdentidades.add( item_modificaCliente = new JMenuItem("Modificar Cliente") ).addActionListener(this);
				menuIdentidades.add( item_registraEmpresaCliente = new JMenuItem("Registrar empresa Cliente") ).addActionListener(this);
				menuIdentidades.add( item_modificaEmpresaCliente = new JMenuItem("Modificar empresa Cliente") ).addActionListener(this);
				menuIdentidades.add( item_registraEmpleado = new JMenuItem("Registrar Empleado") ).addActionListener(this);
				menuIdentidades.add( item_modificaEmpleado = new JMenuItem("Modificar Empleado") ).addActionListener(this);
				menuIdentidades.add( item_registraProveedor = new JMenuItem("Registrar Proveedor") ).addActionListener(this);
				menuIdentidades.add( item_modificaProveedor = new JMenuItem("Modificar Proveedor") ).addActionListener(this);
			//
			barraMenu.add( menuReportes = new JMenu("Reportes") );
				menuReportes.add( new JMenuItem("Reporte de Compras") ).addActionListener(this);
				menuReportes.add( new JMenuItem("Reporte de Compras con Factura") ).addActionListener(this);
				menuReportes.add( new JMenuItem("Reporte de Ventas") ).addActionListener(this);
				menuReportes.add( new JMenuItem("Reporte de Ventas con factura") ).addActionListener(this);
				menuReportes.add( new JMenuItem("Reporte Total de Ventas") ).addActionListener(this);
			//
	}
	/*				*/
	private void tamanioVentana(){
		alto=450;	//PRUEBAS
		ancho=640;	//PRUEBAS
		/*
                //	http://lineadecodigo.com/java/conocer-la-resolucion-de-la-pantalla-con-java/
		Toolkit t = Toolkit.getDefaultToolkit();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		alto = screenSize.height-65;
		ancho = screenSize.width;
		this.setResizable(false);	//	Sería recomendable establecer un tamaño minimo de reduccion de pantalla o dejarlo en false
		if(alto>=703) // 703=768-65
			alto = alto/2;
		if(ancho>=1024)
			ancho = ancho/2;
                */
		imgAlto = alto-195;
		if((imgAlto*1.33)>(ancho-250))
			imgAncho = ancho-250;
		else
			imgAncho = (int)(imgAlto*1.33);
	}
	/*				*/
    
    private JComboBox listaToComboBox(JComboBox comboBox,ArrayList<String> lista){
        for(int i=0;i<lista.size();i++)
            comboBox.addItem(lista.get(i));
        return comboBox;
    }
    
    private JComboBox listaToComboBox(JComboBox comboBox,ArrayList<String> lista,String adicional,boolean posicion){
        if(posicion)
            comboBox.addItem(adicional);
        for(int i=0;i<lista.size();i++)
            if(!adicional.equals(lista.get(i)))
                comboBox.addItem(lista.get(i));
        if(!posicion)
	        comboBox.addItem(adicional);
        return comboBox;
    }
    
    public Alvendi4free_gui() {
		super();
		int i;
		tamanioVentana();
		barraMenus();
		this.setTitle("Alvendi4free ("+alto+"x"+ancho+")"); 	// colocamos titulo a la ventana
		this.setSize(ancho,alto);              	// colocamos tamanio a la ventana (ancho, alto)
		this.setLocationRelativeTo(null);      	// centramos la ventana en la pantalla
		this.setLayout(null);                  	// no usamos ningun layout, solo asi podremos dar posiciones a los componentes
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    // hacemos que cuando se cierre la ventana termina todo proceso
		
		getContentPane().setLayout(null);
		getContentPane().add(evt_P.tamanioTablaInventario = new JScrollPane(evt_P.tablaInventario = new JTable()) );
		getContentPane().add(evt_V.tamanioTablaCarrito = new JScrollPane(evt_V.tablaCarrito = new JTable()) );
		
		this.add( lblID = new JLabel() );
		this.add( lblNombre = new JLabel() );
		this.add( lblActivo = new JLabel() );
		this.add( lbl1 = new JLabel() );
		this.add( lbl2 = new JLabel() );
		this.add( lbl3 = new JLabel() );
		this.add( lbl4 = new JLabel() );
		this.add( lbl5 = new JLabel() );
		this.add( lbl6 = new JLabel() );
		this.add( lbl7 = new JLabel() );
		this.add( lbl8 = new JLabel() );
		this.add( lbl9 = new JLabel() );
		this.add( lbl10 = new JLabel() );
		this.add( lbl11 = new JLabel() );
		this.add( lblImagen = new JLabel() );
		
		this.add( evt_P.cajaBuscar = new JTextField() );
		//Cajas Venta Producto
		this.add( cajaID = new JTextField() );
		this.add( evt_P.cajaCantidad = new JTextField() );
		//Cajas para Capturar Producto
		this.add( evt_P.cajaNombre = new JTextField() );
		this.add( evt_P.cajaModelo = new JTextField() );
		this.add( evt_P.cajaMarca = new JTextField() );
		this.add( evt_P.cajaPrecio = new JTextField() );
		this.add( evt_P.cajaExistencia = new JTextField() );
		this.add( evt_P.cajaImagen = new JTextField() );
		this.add( evt_P.cajaDescripcion = new JTextField() );
		//Cajas para Carrito
		this.add(evt_V.cajaDescuento = new JTextField());
		this.add(evt_V.cajaCupon = new JTextField());
                //Cajas para Rastreos
		this.add(evt_e.cajaPaqueteriaNueva = new JTextField());
		this.add(evt_e.cajaGuiaRastreo = new JTextField());
		this.add(evt_e.cajaDescripcion = new JTextField());
		
		this.add(evt_P.checkActivo = new JCheckBox("ACTIVO"));
		this.add(evt_V.checkFactura = new JCheckBox("Facturar"));
		
		final String[] S_medioEntrega = {"mostrador","envio paqueteria","punto reunion"};
		final String[] S_medioPago = {"efectivo","tarjeta de Debito","tarjeta de Credito","transferencia","deposito","paypal","mercadopago"};
		final String[] S_envioA = {"Ventas","Compras"};
                Calendar cal = Calendar.getInstance();
                
		this.add( comboVentas_medioEntrega = new JComboBox(S_medioEntrega) );
		this.add( comboVentas_medioPago = new JComboBox(S_medioPago) );
		this.add( evt_e.comboEnvios_envioA = new JComboBox(S_envioA) );
		this.add( evt_e.comboEnviosDomicilio = new JComboBox() );
		this.add( evt_e.comboPaqueterias = new JComboBox() );
		this.add( comboTipos = new JComboBox() );
		this.add( comboEmpleadosVentas = new JComboBox() );
		this.add( comboClientesVentas = new JComboBox() );
		this.add( miFecha.cmbMes = new JComboBox(miFecha.meses) );
            miFecha.cmbMes.addActionListener(this);
		this.add( miFecha.cmbDia = new JComboBox() );
            miFecha.cmbDia.addActionListener(this);
            this.add( miFecha.cmbAnio = new JComboBox() );
            miFecha.cmbAnio.addActionListener(this);
		/******************************/
		db.conectar();
            comboTipos = listaToComboBox(comboTipos
                ,db.SELECTlist("DISTINCT tipo","productos"),"otros",true);
            evt_e.comboPaqueterias = listaToComboBox(evt_e.comboPaqueterias
                ,db.SELECTlist("DISTINCT paqueteria","envios"),evt_e.OtraPaqueteria,true);
            evt_e.comboPaqueterias.addActionListener(this);
            comboEmpleadosVentas = listaToComboBox(comboEmpleadosVentas
                ,db.SELECTlist("U.nombre","empleados AS E, usuarios AS U WHERE E.puesto='ventas' AND E.idUsuario=U.idUsuario"));
            comboClientesVentas = listaToComboBox(comboClientesVentas
                ,db.SELECTlist("U.nombre","clientes AS C, usuarios AS U WHERE C.idUsuario=U.idUsuario"));
		db.desconectar();
		/******************************/
        for(i=1;i<=31;i++)
            miFecha.cmbDia.addItem(i);
        for(i=(cal.get(Calendar.YEAR)-1);i<=(cal.get(Calendar.YEAR)+1);i++)
            miFecha.cmbAnio.addItem(i);
		
		this.add( btnBuscar = new JButton("Buscar") );
		btnBuscar.addActionListener(this);
		this.add( btnDetalle = new JButton("Mostrar Detalle") );
		btnDetalle.addActionListener(this);
		this.add( btnMasDetalle = new JButton("Más Detalles") );
		btnMasDetalle.addActionListener(this);
		this.add( btnCarrito = new JButton("Agregar Producto a Carrito de Venta") );
		btnCarrito.addActionListener(this);
		this.add( btnModificar = new JButton("Modificar") );
		btnModificar.addActionListener(this);
		this.add( btnRegistrar = new JButton("Registrar") );
		btnRegistrar.addActionListener(this);
		this.add( btnRegistrarOrden = new JButton("Registrar Orden") );
		btnRegistrarOrden.addActionListener(this);
		this.add( btnConsultarOrden = new JButton("Consultar Orden") );
		btnConsultarOrden.addActionListener(this);
                
		this.add( evt_e.btnRegistrarEnvio = new JButton("Registrar") );
		evt_e.btnRegistrarEnvio.addActionListener(this);
		this.add( evt_e.btnEnvioA = new JButton("Siguiente") );
		evt_e.btnEnvioA.addActionListener(this);
                
        miFecha.fijarFechaActual();
        lbl1.setHorizontalAlignment(SwingConstants.RIGHT);
	}
	
	events_productos evt_P=new events_productos();
	events_ventas evt_V=new events_ventas();
	events_envios evt_e=new events_envios();
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if( item_Salir == e.getSource() ){
			System.exit(0);
		}
		if( item_ConsultaIDproducto == e.getSource() ){
			evt_P.aparecer_ConsultaIDproducto();
		}
		if( item_listaInventario == e.getSource() ){
			evt_P.listaInventario();
		}
		if( item_BuscarProducto == e.getSource() ){
			evt_P.aparecer_Buscar();
		}
		if( item_Capturar == e.getSource() ){
			evt_P.aparecer_camposProducto('C');
		}
		if( btnModificar == e.getSource() ){
			evt_P.aparecer_camposProducto('M');
		}
		if( btnDetalle == e.getSource() ){
			evt_P.obtenerDetalleIDproducto(cajaID.getText());
		}
		if( btnMasDetalle == e.getSource() ){
			evt_P.aparecer_MasDetalle();
		}
		if( btnRegistrar == e.getSource() ){
			evt_P.registrando();
		}
		if( btnBuscar == e.getSource() ){
			evt_P.listaBuscar(evt_P.cajaBuscar.getText(),"productos");
		}
		if( btnCarrito == e.getSource() ){
			evt_V.AgregarCantidadAlCarrito(cajaID.getText(),Integer.parseInt(evt_P.cajaCantidad.getText()),Integer.parseInt(evt_P.existencia));
		}
		if( btnRegistrarOrden == e.getSource() ){
			evt_V.registraOrden();
		}
		if( item_Carrito == e.getSource() ){
			evt_V.aparecer_CapturaOrden();
		}
		if( item_ConsultaOrden == e.getSource() ){
			evt_V.aparecer_ConsultaOrdenID();
		}
		if( btnConsultarOrden == e.getSource() ){
			evt_V.detalle_ConsultaOrdenID(cajaID.getText());
		}
		if( item_AgregarRastreo == e.getSource() ){
			evt_e.aparecer_AgregarRastreo_envioA();
		}
		if( evt_e.btnEnvioA == e.getSource()/*| comboEnvios_envioA == e.getSource()*/){
			evt_e.aparecer_AgregarRastreoDomicilio();
		}
        if( evt_e.comboEnviosDomicilio == e.getSource() ){
			evt_e.domicilioSeleccionado( evt_e.comboEnviosDomicilio.getSelectedIndex() );
        }
        if( miFecha.cmbMes == e.getSource() ){
            while(!fecha.fijaMes(miFecha.cmbMes.getSelectedIndex()+1)){
                miFecha.seleccionaMaximoDia();
            }
        }
        if( miFecha.cmbDia == e.getSource() ){
            if(!fecha.fijaDia(Integer.parseInt(""+miFecha.cmbDia.getSelectedItem())))
                miFecha.seleccionaMaximoDia();
        }
        if( miFecha.cmbAnio == e.getSource() ){
            while(!fecha.fijaAnio(Integer.parseInt(""+miFecha.cmbAnio.getSelectedItem()))){
                miFecha.seleccionaMaximoDia();
            }
        }
        if( evt_e.comboPaqueterias == e.getSource() ){
            evt_e.aparecer_cajaPaqueteriaNueva();
        }
        if( evt_e.btnRegistrarEnvio == e.getSource() ){
            evt_e.registra_NuevoRastreo();
        }
	}
	
	private void desaparecer(){	// desaparecer_compartidos()
		btnBuscar.setBounds(1, 1, 1, 0);
		btnCarrito.setBounds( 1, 1, 1, 0);
		btnConsultarOrden.setBounds( 1, 1, 1, 0);
		btnDetalle.setBounds(1, 1, 1, 0);
		btnModificar.setBounds( 1, 1, 1, 0);
		btnMasDetalle.setBounds( 1, 1, 1, 0);
		btnRegistrar.setBounds(1, 1, 1, 0);
		btnRegistrarOrden.setBounds(1, 1, 1, 0);
                cajaID.setBounds( 1, 1, 1, 0);		//	cajaID.setText("");
		comboClientesVentas.setBounds( 1, 1, 1, 0);
		comboEmpleadosVentas.setBounds( 1, 1, 1, 0);
		comboVentas_medioEntrega.setBounds( 1, 1, 1, 0);
		comboVentas_medioPago.setBounds( 1, 1, 1, 0);
		comboTipos.setBounds( 1, 1, 1, 0);		comboTipos.setSelectedItem("otros");
		lblActivo.setBounds( 1, 1, 1, 0);	lblActivo.setText("");
		lbl1.setBounds( 1, 1, 1, 0);		lbl1.setText("");
		lbl2.setBounds( 1, 1, 1, 0);		lbl2.setText("");
		lbl3.setBounds( 1, 1, 1, 0);		lbl3.setText("");
		lbl4.setBounds( 1, 1, 1, 0);		lbl4.setText("");
		lbl5.setBounds( 1, 1, 1, 0);		lbl5.setText("");
		lbl6.setBounds( 1, 1, 1, 0);		lbl6.setText("");
		lbl7.setBounds( 1, 1, 1, 0);		lbl7.setText("");
		lbl8.setBounds( 1, 1, 1, 0);		lbl8.setText("");
		lbl9.setBounds( 1, 1, 1, 0);		lbl9.setText("");
		lbl10.setBounds( 1, 1, 1, 0);		lbl10.setText("");
		lbl11.setBounds( 1, 1, 1, 0);		lbl11.setText("");
		lblID.setBounds( 1, 1, 1, 0);		lblID.setText("");
		lblImagen.setBounds( 1, 1, 1, 0);
		lblNombre.setBounds( 1, 1, 1, 0);	lblNombre.setText("");
        lbl1.setHorizontalAlignment(SwingConstants.LEFT);
        lbl2.setHorizontalAlignment(SwingConstants.LEFT);
        lbl3.setHorizontalAlignment(SwingConstants.LEFT);
        lbl4.setHorizontalAlignment(SwingConstants.LEFT);
        lbl5.setHorizontalAlignment(SwingConstants.LEFT);
        lbl6.setHorizontalAlignment(SwingConstants.LEFT);
        //  PRODUCTOS    //
        evt_P.productos_desaparecer();
        //  VENTAS  //
		evt_V.tamanioTablaCarrito.setBounds( 1, 1, 1, 0);
        evt_V.cajaDescuento.setBounds( 1, 1, 1, 0);     evt_V.cajaDescuento.setText("");
		evt_V.cajaCupon.setBounds( 1, 1, 1, 0);		evt_V.cajaCupon.setText("");
		evt_V.checkFactura.setBounds( 1, 1, 1, 0);
        //  ENVIOS  //
        evt_e.envios_desaparecer();
		//  FECHA   //
        miFecha.fecha_desaparecer();
	}
	
	private class miFecha{
        final String[] meses = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
        private JComboBox cmbDia,cmbMes,cmbAnio;
        
        public void fecha_desaparecer(){
            cmbDia.setBounds( 1, 1, 1, 0);
            cmbMes.setBounds( 1, 1, 1, 0);
            cmbAnio.setBounds( 1, 1, 1, 0);
        }
        private void fijarFechaActual(){
            fecha.fijarFechaActual();
            cmbAnio.setSelectedItem(fecha.dameAnio());
            cmbMes.setSelectedIndex(fecha.dameMes()-1);
            cmbDia.setSelectedItem(fecha.dameDia());
        }
        private void seleccionaMaximoDia(){
            String mostrar=("En el mes de "+meses[cmbMes.getSelectedIndex()]+" solo hay "+fecha.dameMaxDia()+" dias");
            if( cmbMes.getSelectedIndex()==1 && fecha.verificaBisiesto(Integer.parseInt(""+cmbAnio.getSelectedItem())) )
                mostrar = mostrar+" en año bisiesto";
            elemGraf.INFO(mostrar);
            fecha.fijaDia(fecha.dameMaxDia());
            cmbDia.setSelectedItem(fecha.dameDia());
        }
    }
    
    private class events_ventas{
            DefaultTableModel modelo;
            JTable tablaCarrito;
            JScrollPane tamanioTablaCarrito;
            private int margenY;
            JTextField cajaDescuento,cajaCupon;
            JCheckBox checkFactura;
            ArrayList<String> listaID = new ArrayList<>();
            ArrayList<String> listaCantidad = new ArrayList<>();
		
		private void listaCarrito(ArrayList<String> listaID,ArrayList<String> listaCantidad,ArrayList<String> listaPrecioVigente,int X,int Y,int W,int H){
			int mitad = (ancho/2);
			Float precioLista;
			lbl1.setText("Pago:");
			lbl2.setText("Entrega:");
			lbl3.setText("Descuento: $");
			lbl4.setText("Cupon:");
			lbl1.setBounds( 10,Y+H, 45, 25);
			lbl2.setBounds(mitad,Y+H, 68, 25);
			lbl4.setBounds(mitad,Y+H+30,60,25);
			lbl3.setBounds(mitad,Y+H+55,100, 25);			//Deshacerme de Descuento ?
			/*		Datos y Columnas de Tabla		*/
			tamanioTablaCarrito.setBounds( X, Y,W,H);
			String[] columnas = {"ID","Nombre","Cantidad","$ unitario","Subtotal"};
			modelo = new DefaultTableModel();
			modelo.setColumnIdentifiers(columnas);
			db.conectar();
			for(int p=0;listaID.size()>0 && p<listaID.size();p++){
				ArrayList<String> listaApoyo = db.SELECTlist("nombre,modelo,marca,precio","productos WHERE idProducto="+listaID.get(p));
				Object[] fila = new Object[columnas.length];
				fila[0] = listaID.get(p);
				fila[1] = listaApoyo.get(0)+" "+listaApoyo.get(1)+" "+listaApoyo.get(2);
				if(listaPrecioVigente.isEmpty() | listaPrecioVigente.size()!=listaID.size() )
					precioLista = Float.parseFloat(listaApoyo.get(3));
				else
					precioLista = Float.parseFloat(listaPrecioVigente.get(p));
				fila[2] = listaCantidad.get(p);
				fila[3] = "$ "+precioLista;
				fila[4] = "$ "+String.format("%.02f",precioLista*Integer.parseInt(listaCantidad.get(p)) );
				modelo.addRow(fila);
				listaApoyo.clear();
			}
			tablaCarrito.updateUI();
			db.desconectar();
			/*		Propiedades de Tabla		*/
			tablaCarrito.setModel(modelo);
			tablaCarrito.setEnabled(true); // Editable
			tablaCarrito.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS); // JTable.AUTO_RESIZE_OFF);
			tablaCarrito.setFillsViewportHeight(true);
		}
		private void AgregarCantidadAlCarrito(String id,int cantidad,int existencia){
			if( existencia>0 && cantidad>0 ){
				if(listaID.contains(id)){
					int p = listaID.indexOf(id);
					cantidad = cantidad + Integer.parseInt(listaCantidad.get(p));
					if(cantidad<=existencia)
						listaCantidad.set(p,Integer.toString(cantidad));
					else
						listaCantidad.set(p,Integer.toString(existencia));
				}else{
					listaID.add(id);
					if(cantidad<=existencia)
						listaCantidad.add(Integer.toString(cantidad));
					else
						listaCantidad.add(Integer.toString(existencia));
				}
				if(cantidad<=existencia)
					elemGraf.INFO("Se ha agregado el producto al Carrito");
				else
					elemGraf.WARNING("No hay suficiente stock, se ha incluido el remanente en Carrito");
			}else if(existencia<=0)
				elemGraf.ERROR("No hay stock de este producto");
			else if(cantidad<=0)
				elemGraf.ERROR("Indicame una cantidad >0");
		}
		private void aparecer_CapturaOrden(){
			int mitad = ancho/2;
			margenY = (alto/2)+5;
			desaparecer();
			cajaDescuento.setText("0.0");		//Deshacerme de Descuento ?
			listaCarrito(listaID,listaCantidad,new ArrayList<String>(), 5, 5,ancho-10,margenY-5);
			comboVentas_medioPago.setBounds( 58,margenY,mitad-65, 25);
			comboVentas_medioEntrega.setBounds(mitad+68,margenY,mitad-78, 25);
			cajaCupon.setBounds(mitad+60,margenY+30,mitad-70, 25);
			cajaDescuento.setBounds(mitad+100,margenY+55,mitad-115, 25);		//Deshacerme de Descuento ?
			comboClientesVentas.setBounds(10,margenY+30,mitad-20,28);
			comboEmpleadosVentas.setBounds(10,margenY+60,mitad-20,28);
			btnRegistrarOrden.setBounds(mitad,margenY+80,mitad-20,28);
		}
		private void registraOrden(){
			int idEmpleado_vende,idCliente_compra,ordenFolio,existencia;
			if(listaID.size()>0){
				db.conectar();
				ArrayList<String> fh = db.fechaYhora();
				String fecha = fh.get(0);
				String hora = fh.get(1);
				idEmpleado_vende = db.SELECT_ID("idUsuario","usuarios WHERE nombre='"+comboEmpleadosVentas.getSelectedItem()+"'");
				idCliente_compra = db.SELECT_ID("idUsuario","usuarios WHERE nombre='"+comboClientesVentas.getSelectedItem()+"'");
				db.INSERT("ordenesVentas (idEmpleado_vende,idCliente_compra,fecha,hora,medioEntrega,medioPago,descuento)",
					"("+idEmpleado_vende+","+idCliente_compra+",'"+fecha+"','"+hora+"','"+comboVentas_medioEntrega.getSelectedItem()
					+"','"+comboVentas_medioPago.getSelectedItem()+"','"+cajaDescuento.getText()+"')");
				ordenFolio = db.SELECT_ID("ordenFolio","ordenesVentas WHERE fecha='"+fecha+"' AND hora='"+hora
					+"' AND idEmpleado_vende="+idEmpleado_vende+" AND idCliente_compra="+idCliente_compra );
				for(int p=0;listaID.size()>0 && p<listaID.size();p++){
					db.INSERT("productosVendidos","("+ordenFolio+","+listaID.get(p)+","+listaCantidad.get(p)+",0,(SELECT precio FROM productos WHERE idProducto="+listaID.get(p)+") )");
					db.UPDATE("productos","existencia=existencia-"+listaCantidad.get(p),"idProducto="+listaID.get(p));
				}
				db.desconectar();
				listaID.clear();
				listaCantidad.clear();
				aparecer_CapturaOrden();
				elemGraf.INFO("Orden Registrada con el Folio: "+ordenFolio);
			}else
				elemGraf.ERROR("No hay pedido por registrar");
		}
		private void aparecer_ConsultaOrdenID(){
			desaparecer();
			lblID.setText("Numero de Orden:");
			lblID.setBounds( 33, 10,200, 25);
			cajaID.setBounds(175, 10, 75, 25);
			btnConsultarOrden.setBounds(260, 10,160, 28);
		}
		private void detalle_ConsultaOrdenID(String folio){
			ArrayList<String> listaID_enOrden = new ArrayList<>();
			ArrayList<String> listaCantidad_enOrden = new ArrayList<>();
			ArrayList<String> listaPrecioVigente = new ArrayList<>();
            ArrayList<String> listaApoyo;
            String medioPago,medioEntrega,descuento;
            
			aparecer_ConsultaOrdenID();
			db.conectar();
			if(db.SELECT("COUNT(*)","ordenesVentas WHERE ordenFolio="+folio).equals("0"))
				elemGraf.INFO("No existe la orden "+folio);
			else{
                        medioPago = db.SELECT("medioPago","ordenesVentas WHERE ordenFolio="+folio);
                        medioEntrega = db.SELECT("medioEntrega","ordenesVentas WHERE ordenFolio="+folio);
                        descuento = db.SELECT("descuento","ordenesVentas WHERE ordenFolio="+folio);
                        listaApoyo = db.SELECTlist("idProducto,cantidad,precioVigente","productosVendidos WHERE ordenFolio="+folio);
                        for(int p=0;p<listaApoyo.size();p+=3){
                            listaID_enOrden.add(listaApoyo.get(p));
                            listaCantidad_enOrden.add(listaApoyo.get(p+1));
                            listaPrecioVigente.add(listaApoyo.get(p+2));
                        }
                        listaCarrito(listaID_enOrden,listaCantidad_enOrden,listaPrecioVigente, 5, 40,ancho-10,(alto/2)-30);
                        listaApoyo.clear();
                        lbl1.setText("Pago: "+medioPago);
                        lbl2.setText("Entrega: "+medioEntrega);
                        lbl3.setText("Descuento: $"+descuento);
                        //lbl4.setText("Cupon: "+listaApoyo.get(9));
                        lbl1.setSize((ancho/2)-10,25);
                        lbl2.setSize((ancho/2)-10,25);
                        lbl3.setSize((ancho/2)-10,25);
			}
			db.desconectar();
		}
	}
	private class events_productos{
        private char modoR;
        JTable tablaInventario;
        JScrollPane tamanioTablaInventario;
        private JCheckBox checkActivo;
        JTextField cajaBuscar,cajaCantidad,cajaNombre,cajaModelo,cajaMarca;
        JTextField cajaPrecio,cajaExistencia,cajaImagen,cajaDescripcion;
        private String nombre,marca,modelo,existencia,tipo,rutaImagen,imagen,activo,descripcion;
        private double precio;
        
        public void productos_desaparecer(){
			tamanioTablaInventario.setBounds( 1, 1, 1, 0);
		    checkActivo.setBounds( 1, 1, 1, 0);
		    cajaImagen.setBounds( 1, 1, 1, 0);            cajaImagen.setText("");
		    cajaBuscar.setBounds( 1, 1, 1, 0);
		    cajaCantidad.setBounds( 1, 1, 1, 0);
		    cajaDescripcion.setBounds( 1, 1, 1, 0);       cajaDescripcion.setText("");
		    cajaExistencia.setBounds( 1, 1, 1, 0);        cajaExistencia.setText("");
		    cajaMarca.setBounds( 1, 1, 1, 0);             cajaMarca.setText("");
		    cajaModelo.setBounds( 1, 1, 1, 0);            cajaModelo.setText("");
		    cajaNombre.setBounds( 1, 1, 1, 0);            cajaNombre.setText("");
		    cajaPrecio.setBounds( 1, 1, 1, 0);            cajaPrecio.setText("");
        }
		
		private void aparecer_ConsultaIDproducto(){
			desaparecer();
			lblID.setText("ID del producto:");
			lblID.setBounds(50, 10,180, 25);
			cajaID.setBounds(175, 10, 75, 25);
			btnDetalle.setBounds(260, 10,150, 28);
		}
		
		private void definir_lblIDproducto(){	//Ordenados en orden descendente
			lblNombre.setText("Nombre: ");
			lbl1.setText("Tipo: ");
			lbl2.setText("Normal");		
			lbl4.setText("Precio:  $  ");
			lbl5.setText("$");
			lbl6.setText("Existencia:  ");
		}
		
		@SuppressWarnings("empty-statement")
		private void aparecer_DetalleIDproducto(){	//Ordenados en orden descendente
			aparecer_ConsultaIDproducto();
			definir_lblIDproducto();
			lblNombre.setBounds( 10, 40,ancho-20, 25);	lblNombre.setText( lblNombre.getText()+nombre+" "+modelo+" "+marca );
			lblActivo.setBounds( 10, 65, 90, 25);	lblActivo.setText(activo);
			lbl1.setBounds(100, 65,200, 25);		lbl1.setText( lbl1.getText()+tipo );
			lbl2.setBounds( 85, 90,100, 25);
			lbl3.setBounds(150, 90,120, 25);
			lbl4.setBounds( 10,115,140, 25);		lbl4.setText( lbl4.getText()+String.format("%.02f",precio) );
			lbl6.setBounds( 10,140,100, 25);		lbl6.setText( lbl6.getText()+existencia );
			lblImagen = elemGraf.lblImagen(lblImagen,rutaImagen,ancho-10-imgAncho,65,imgAncho,imgAlto);
			lbl10.setBounds( 10,alto-125, 85, 25);	lbl10.setText("Cantidad ");
			cajaCantidad.setBounds( 85,alto-125, 85, 25);
			btnCarrito.setBounds(180,alto-125,ancho-200, 28);
			btnMasDetalle.setBounds(10,alto-90,(ancho/2)-20, 28);
			btnModificar.setBounds(ancho/2,alto-90,(ancho/2)-20, 28);
			switch(tipo){
				case "pcb": 	break;
				case "ram": 	break;
				case "almacenamiento": break;
				case "unidadOptica": break;
				case "computadoras": break;
				default: 	btnMasDetalle.setBounds( 1, 1, 1, 0);
			};
		}
		
		private void aparecer_MasDetalle(){
			evt_P.aparecer_ConsultaIDproducto();
			evt_P.aparecer_DetalleIDproducto();
			db.conectar();
			elemGraf.INFO(db.SELECT("*",tipo+" WHERE idProducto='"+cajaID.getText()+"'"));
			db.desconectar();
		}
		
		private void aparecer_camposProducto(char c){	// Para Capturar o Modificar un Producto
			modoR = c;
			desaparecer();
			definir_lblIDproducto();
			lbl7.setText("Modelo: ");
			lbl8.setText("Marca:  ");
			lbl9.setText("Imagen: ");
			lbl10.setText("Descripcion: ");
			lblNombre.setBounds( 10, 10, 70, 25);
			cajaNombre.setBounds( 80, 10,ancho-100, 25);
			checkActivo.setBounds( 15, 35, 75, 25);		//	checkActivo.addChangeListener(this);
			lbl7.setBounds((ancho/3)-25, 35, 70, 25);
			cajaModelo.setBounds((ancho/3)+35, 35,(ancho/3)-60, 25);
			lbl8.setBounds((ancho/3*2)-15, 35, 70, 25);
			cajaMarca.setBounds((ancho/3*2)+40, 35,(ancho/3)-60, 25);
			lbl9.setBounds( 10, 60, 65, 25);
			cajaImagen.setBounds( 75, 60,(ancho/2)-10, 25);
			lbl2.setBounds( (ancho/2)+ 75, 60,100, 25);
			lbl3.setBounds( (ancho/2)+140, 60,120, 25);
			lbl4.setBounds( ancho/2, 85, 80, 25);
			lbl5.setBounds( (ancho/2)+140,85,100, 25);
			lbl6.setBounds( ancho/2,110,100, 25);
			lbl1.setBounds( 10, 85,50, 25);
			comboTipos.setBounds( 55, 85,160, 25);
			lbl10.setBounds(10,135,100, 25);
			cajaDescripcion.setBounds(105,135,ancho-125, 25);
			cajaPrecio.setBounds ( (ancho/2)+ 70, 85, 70, 25);
			cajaExistencia.setBounds( (ancho/2)+ 85,110, 50, 25);
			btnRegistrar.setBounds( 20,alto-95,200, 28);
			if(modoR=='C'){
				btnRegistrar.setText("Guardar Captura");
				checkActivo.setSelected(true);
			} else
			if(modoR=='M'){
				btnRegistrar.setText("Guardar Modificacion");
                switch (activo) {
                    case "ACTIVO":
                        checkActivo.setSelected(true);
                        break;
                    case "INACTIVO":
                        checkActivo.setSelected(false);
                        break;
                }
				comboTipos.setSelectedItem(tipo);
				cajaNombre.setText(nombre);
				cajaModelo.setText(modelo);
				cajaMarca.setText(marca);
				cajaPrecio.setText(String.format("%.02f",precio));
				cajaExistencia.setText(existencia);
				cajaImagen.setText(imagen);
				cajaDescripcion.setText(descripcion);
			}
		}
		
		private void aparecer_Buscar(){
			desaparecer();
			lbl1.setText("Palabra de Busqueda:");
			lbl1.setBounds(10, 10,220, 25);
			cajaBuscar.setBounds(175, 10, 75, 25);
			btnBuscar.setBounds(260, 10,150, 28);
		}
		
		private void listaBuscar(String txtBuscado,String tabla){
			String[] columnas = {"ID","Nombre","Modelo","Marca","Tipo","Existencia"};
			tamanioTablaInventario.setBounds( 5, 40,ancho-10,alto-110);
			db.conectar();
			tablaIDseleccionable(columnas,db.SELECTlist("idProducto,nombre,modelo,marca,tipo,existencia",tabla+" WHERE (nombre LIKE '%"
				+txtBuscado+"%' OR modelo LIKE '%"+txtBuscado+"%'OR marca LIKE '%"+txtBuscado+"%'OR tipo LIKE '%"+txtBuscado+"%') AND activo='1'"));
			db.desconectar();
		}
		
		private void listaInventario(){
			desaparecer();
			String[] columnas = {"ID","Nombre","Modelo","Marca","Tipo","Existencia"};
			tamanioTablaInventario.setBounds( 5, 5,ancho-10,alto-70);
			db.conectar();
			tablaIDseleccionable(columnas,db.SELECTlist("idProducto,nombre,modelo,marca,tipo,existencia","productos WHERE activo='1'"));
			db.desconectar();
		}
		
		private void tablaIDseleccionable(String[] columnas,ArrayList<String> listaApoyo){
			DefaultTableModel modeloL;
                        modeloL = new DefaultTableModel();
			modeloL.setColumnIdentifiers(columnas);
			int registros= listaApoyo.size()/columnas.length;
			for(int p=0;p<registros;p++){
				Object[] fila = new Object[columnas.length];
				for(int q=0;q<columnas.length;q++)
					fila[q] = listaApoyo.get((p*columnas.length)+q);
				modeloL.addRow(fila);
			}
			tablaInventario.updateUI();
			/*		Propiedades de Tabla		*/
			tablaInventario.setModel(modeloL);
			tablaInventario.setEnabled(true); // Editable	// Siendo editable será posible seleccionar un item del inventario
			tablaInventario.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS); // JTable.AUTO_RESIZE_OFF);
			tablaInventario.doLayout();
			tablaInventario.setFillsViewportHeight(true);
			//setEventoMouseClicked(tablaInventario);
			
			tablaInventario.addMouseListener(new java.awt.event.MouseAdapter() {
				
				@Override
				public void mouseClicked(MouseEvent e) {
					desaparecer();
					String value = ""+tablaInventario.getValueAt(tablaInventario.getSelectedRow(),0);
					cajaID.setText(value);
					obtenerDetalleIDproducto(""+value);
					aparecer_DetalleIDproducto();
				}
				
			});
		}
		
		private void obtenerDetalleIDproducto(String id){
			aparecer_ConsultaIDproducto();
			db.conectar();
			if(db.SELECT("COUNT(*)","productos WHERE idProducto="+id).equals("0"))
				elemGraf.ERROR("No existe el ID "+id);
			else{
				ArrayList<String> columnas = db.SELECTlist("imagen,nombre,modelo,marca,existencia,tipo,descripcion,precio,activo","productos WHERE idProducto="+id);
				imagen 		=	columnas.get(0);
				nombre		=	columnas.get(1);
				modelo		=	columnas.get(2);
				marca		=	columnas.get(3);
				existencia	=	columnas.get(4);
				tipo		=	columnas.get(5);
				descripcion	=	columnas.get(6);
				precio	= Float.parseFloat(columnas.get(7));
				activo		=	columnas.get(8);
				rutaImagen = DIR_IMG+imagen;
                switch (activo) {
					case "1":
						activo="ACTIVO";
						lblActivo.setForeground(Color.GREEN);
						break;
					case "0":
						activo="INACTIVO";
						lblActivo.setForeground(Color.RED);
						break;
				}
				aparecer_DetalleIDproducto();
			}
			db.desconectar();
		}
		
		@SuppressWarnings("empty-statement")
		private void registrando(){
			//Recuperar Valores actuales de Producto si se desea modificar
			if (checkActivo.isSelected()==true)
				activo="1";
			else if (checkActivo.isSelected()==false)
				activo="0";
			tipo = comboTipos.getSelectedItem().toString();
			nombre = cajaNombre.getText();
			modelo = cajaModelo.getText();
			marca = cajaMarca.getText();
			precio = Float.parseFloat( cajaPrecio.getText() );
			existencia = cajaExistencia.getText();
			imagen = cajaImagen.getText();
			descripcion = cajaDescripcion.getText();
			switch(tipo){
				case "pcb":
					elemGraf.INFO("PROBANDO PCB");
					break;
				case "ram":
					elemGraf.INFO("PROBANDO RAM");
					break;
				case "almacenamiento":
					elemGraf.INFO("PROBANDO almacenamiento");
					break;
				case "unidadOptica":
					elemGraf.INFO("PROBANDO unidadOptica");
					break;
				case "computadoras":
					elemGraf.INFO("PROBANDO computadoras");
					break;
			};
			// Insertar o Actualizar Registro
			db.conectar();
			String tablaCampos="productos(nombre,modelo,marca,precio,existencia,imagen,descripcion,tipo)";
			String datosCampos="('"+nombre+"','"+modelo+"','"+marca+"','"+precio+"','"+existencia+"','"+imagen+"','"+descripcion+"','"+tipo+"')";
			if(modoR=='C'){
				if( db.SELECT("COUNT(*)","productos WHERE nombre='"+nombre+"' AND modelo='"+modelo+"' AND marca='"+marca
					+"' AND imagen='"+imagen+"' AND descripcion='"+descripcion+"' AND tipo='"+tipo+"'").equals("0") )
				{
					db.INSERT(tablaCampos,datosCampos);
					elemGraf.INFO("Se ha registrado el producto"+nombre);
				}else
					elemGraf.WARNING("Este registro ya existe");
			}
			if(modoR=='M'){
				db.UPDATE("productos","nombre='"+nombre+"',modelo='"+modelo+"',marca='"+marca+"',precio='"+precio
					+"',existencia='"+existencia+"',imagen='"+imagen+"',descripcion='"+descripcion+"',tipo='"+tipo+"'"
					, "idProducto='"+cajaID.getText()+"'");
				elemGraf.INFO("Producto ID "+cajaID.getText()+" ha sido Modificado");
			}
			db.desconectar();
		}
		
	}
	
	private class events_envios{
		public JButton btnEnvioA,btnRegistrarEnvio;
		public JComboBox comboEnvios_envioA,comboEnviosDomicilio,comboPaqueterias;
		public JTextField cajaGuiaRastreo,cajaPaqueteriaNueva,cajaDescripcion;
		private final String OtraPaqueteria = "Otra Paqueteria";
		private final String nuevoDomicilio = "Registrar NUEVO domicilio";
		private ArrayList<Integer> indiceDomicilios = new ArrayList();
		private int idUsuario,xPN,yPN,anchoPN;
		
		private void envios_desaparecer(){
			miFecha.fecha_desaparecer();
			btnRegistrarEnvio.setBounds(1, 1, 1, 0);
			comboEnvios_envioA.setBounds( 1, 1, 1, 0);
		    comboEnviosDomicilio.setBounds( 1, 1, 1, 0);
            comboPaqueterias.setBounds( 1, 1, 1, 0);
            cajaDescripcion.setBounds( 1, 1, 1, 0);         cajaDescripcion.setText("");
            cajaGuiaRastreo.setBounds( 1, 1, 1, 0);         cajaGuiaRastreo.setText("");
            cajaPaqueteriaNueva.setBounds( 1, 1, 1, 0);     cajaPaqueteriaNueva.setText("");
        }
        
        private void aparecer_AgregarRastreo_envioA(){
			desaparecer();
			comboEnvios_envioA.setBounds(15, 10,125, 25);
		    lblID.setText("ID de Usuario:");
		    lblID.setBounds(150, 10,180, 25);
		    cajaID.setBounds(270, 10, 75, 25);
		    btnEnvioA.setBounds(360, 10,150, 28);
        }
        
        private void aparecer_AgregarRastreoDomicilio(){
			int i,r,nDomicilios;
            boolean existeUsuario = false;
		    ArrayList<String> domicilios = new ArrayList();
		    String tablasYfiltros, envioA = comboEnvios_envioA.getSelectedItem().toString();
            idUsuario = Integer.parseInt(cajaID.getText());
        	tablasYfiltros =" AS U, usuariosDomicilios AS UD, domicilio AS D WHERE UD.idUsuario="+idUsuario+" AND D.idDomicilio=UD.idDomicilios";
            /**********************************************************/
		    db.conectar();
		    switch (envioA) {
                    case "Ventas":
                        tablasYfiltros = "clientes"+tablasYfiltros;
                        existeUsuario = db.SELECT_existe("clientes WHERE idUsuario="+idUsuario);
                        break;
                    case "Compras":
                        tablasYfiltros = "empleados"+tablasYfiltros+" AND U.idUsuario=UD.idUsuario";
                        existeUsuario = db.SELECT_existe("empleados WHERE idUsuario="+idUsuario);
                        break;
            }
            if(existeUsuario){
                    lbl1.setText( db.SELECT("nombre,apePat,apeMat", "usuarios WHERE idUsuario="+idUsuario) );
                    comboEnviosDomicilio.removeAllItems();
                    if(idUsuario!=0){   //Que si no se trata de Publico en General
                        domicilios = db.SELECTlist("D.idDomicilio,D.calleNumero,D.colonia,D.cp,D.municipio,D.pais",tablasYfiltros);
                        nDomicilios = Integer.parseInt(db.SELECT("count(*)",tablasYfiltros));
                        indiceDomicilios.clear();
                        for(i=0;i<domicilios.size();i+=(domicilios.size()/nDomicilios)){
                            indiceDomicilios.add(Integer.parseInt(domicilios.get(i)));
                            envioA = domicilios.get(i+1);
                            for(r=2;r<(domicilios.size()/nDomicilios);r++){
                                if(!domicilios.get(i+r).equals("null"))
                                    envioA+=(", "+domicilios.get(i+r));
                            }
                            comboEnviosDomicilio.addItem(envioA);
                        }
                    }
                    comboEnviosDomicilio.addItem(nuevoDomicilio);
                    miFecha.fijarFechaActual();
                    lbl2.setText("Paqueteria:");
                    lbl3.setText("Guia de Rastreo:");
                    lbl4.setText("Año:");
                    lbl5.setText("Descripción:");
                    
                    lbl1.setBounds(10, 40,ancho-20, 28);
                    comboEnviosDomicilio.setBounds(10, 70,ancho-20, 28);
                    lbl2.setBounds(10,100,(ancho/3)-20, 28);
                    comboPaqueterias.setBounds(xPN=ancho/3,yPN=100,anchoPN=(ancho/3)-10, 28);
                    aparecer_cajaPaqueteriaNueva();  // Para colocar aquí cajaPaqueteriaNueva
                    lbl3.setBounds(10,130,(ancho/3)-20, 28);
                    cajaGuiaRastreo.setBounds(ancho/3,130,(ancho/3)-10, 28);
                    miFecha.cmbDia.setBounds(10,160,(ancho/3)-20, 28);
                    miFecha.cmbMes.setBounds((ancho/3)-5,160,(ancho/3)-20, 28);
                    lbl4.setBounds((ancho*2)/3,160,ancho/6, 28);
                    miFecha.cmbAnio.setBounds(((ancho*2)/3)+50,160,ancho/6, 28);
                    lbl5.setBounds(10,190,90, 28);
                    cajaDescripcion.setBounds(100,190,ancho-110, 28);
                    btnRegistrarEnvio.setBounds(ancho/3,230,ancho/3, 28);
                    
                    lbl2.setHorizontalAlignment(SwingConstants.RIGHT);
                    lbl3.setHorizontalAlignment(SwingConstants.RIGHT);
            }else{
                    desaparecer();
                    aparecer_AgregarRastreo_envioA();
            }
            db.desconectar();
            /**********************************************************/
        }
        
        private void aparecer_cajaPaqueteriaNueva(){
                if( OtraPaqueteria.equals(""+comboPaqueterias.getSelectedItem()) ){
                    cajaPaqueteriaNueva.setBounds(xPN+anchoPN,yPN,anchoPN,28);
                }else
                    cajaPaqueteriaNueva.setBounds(1, 1, 1, 0);
        }
        
        void domicilioSeleccionado(int indice){
                int idDomicilio = indiceDomicilios.get(indice);
                if(idDomicilio==-1)
                    elemGraf.WARNING("No existe una opción");
                if(nuevoDomicilio==comboEnviosDomicilio.getSelectedItem())
                    elemGraf.INFO("Saltar a registro de domicilio para usuario"+idUsuario);
                elemGraf.INFO(""+comboEnviosDomicilio.getSelectedItem());
        }
        
        private void registra_NuevoRastreo(){
                int idDomicilio = indiceDomicilios.get(comboEnviosDomicilio.getSelectedIndex());
                String paqueteriaElegida = ""+comboPaqueterias.getSelectedItem();
                String fecha = ""+miFecha.cmbAnio.getSelectedItem()+"-"+(miFecha.cmbMes.getSelectedIndex()+1)+"-"+miFecha.cmbDia.getSelectedItem();
                if(OtraPaqueteria.equals(paqueteriaElegida))
                    paqueteriaElegida = cajaPaqueteriaNueva.getText();
                db.conectar();
                elemGraf.INFO("("+idDomicilio+",'"+paqueteriaElegida+"','"+cajaGuiaRastreo.getText()+"','"+fecha+"')");
                db.INSERT("envios(idDomicilio,paqueteria,guiaRastreo,fechaEnvio)"
                        ,"("+idDomicilio+",'"+paqueteriaElegida+"','"+cajaGuiaRastreo.getText()+"','"+fecha+"')");
                db.desconectar();
        }
        
	}
	
}

public class Alvendi4free {
	
	public static void probandoFuncionesMySQL(){
		funcionesMySQL db = new funcionesMySQL();
        db.conectar();
		//db.CREATE("TABLE cosa(id INT AUTO_INCREMENT, PRIMARY KEY(id), nombre VARCHAR(20), apellidos VARCHAR(20), telefono VARCHAR(20));");
		//db.DROP("TABLE cosa");
		//db.INSERT("productos(idProducto,tipo,nombre)","(4,'otros', 'Raspberry PiOr'),(5,'otros', 'MenOr')");
		//db.DELETE("productos","idProducto=5");
		//db.UPDATE("productos","marca='KFC',precio='99.95'","idProducto=4");
		//System.out.println(	db.SELECT("count(*)","productos WHERE activo=1")	);
		System.out.println(	db.SELECT("nombre,marca,precio","productos WHERE idProducto=1")	);
		db.desconectar();
	}
	
    public static void main(String[] args) {
		/*elementosGraficos elemGraf=new elementosGraficos();
		Alvendi4free_gui V = new Alvendi4free_gui();      // creamos una ventana
		V.setVisible(true);             // hacemos visible la ventana creada
		*/
		probandoFuncionesMySQL();
		//elemGraf.descargaImagen("http://computaccion.com/"+"img/EnvioDHL.jpg","img/EnvioDHL.jpg");	// Hace falta hacer una funcion que genere los directorios
    }
    
}
